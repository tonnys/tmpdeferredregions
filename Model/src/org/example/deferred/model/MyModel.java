package org.example.deferred.model;

import java.util.LinkedList;
import java.util.List;

public class MyModel {
  
  private List<MyObject> myObjects = new LinkedList<MyObject>();
  
  public MyModel() {
    super();
    myObjects.add(new MyObject("x1", "y2"));
    myObjects.add(new MyObject("x2", "y3"));
  }
  
  public List<MyObject>  getMyObjects(){
    return myObjects;
  }
  
  public List<MyObject> fetchMyBunchOfObjects(){
    System.out.println("Fetching my bunch of objects.");
    List<MyObject> list = new LinkedList<MyObject>();
    myObjects.add(new MyObject("m1", "y2"));
    myObjects.add(new MyObject("m2", "y3"));
    return list;
  }
  public List<MyObject> fetchMyObjects(){
    System.out.println("Fetching objects.");
    List<MyObject> list = new LinkedList<MyObject>();
    myObjects.add(new MyObject("z1", "y2"));
    myObjects.add(new MyObject("z2", "y3"));
    return list;
  }
  
  public class MyObject{
    private String value1, value2;
    MyObject(String value1, String value2){
      this.value1 = value1;
      this.value2 = value2;
    }

    public void setValue1(String value1) {
      this.value1 = value1;
    }

    public String getValue1() {
      return value1;
    }

    public void setValue2(String value2) {
      this.value2 = value2;
    }

    public String getValue2() {
      return value2;
    }
  }
}
